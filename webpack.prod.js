const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
  devtool: 'none',
  mode: 'production',
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/config\.dev\.js/, './config.prod.js'),
    new HtmlWebpackPlugin({ title: 'Kaleidoscope' }),
  ],
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
});
