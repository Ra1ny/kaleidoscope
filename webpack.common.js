const path = require('path');
const basePath = p => path.resolve(__dirname, p);

module.exports = {
  entry: [
    basePath('src/js/main.js')
  ],
  output: {
    path: basePath('dist'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: basePath('')
  },
  watchOptions: {
    ignored: /node_modules/,
    poll: true
  }
};
