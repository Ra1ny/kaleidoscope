export default class Kaleidoscope {
  constructor (options = {}, presetImages) {
    this.HALF_PI = Math.PI / 2;
    this.TWO_PI = Math.PI * 2;
    this.viewportHeight = Math.max(
      document.documentElement.clientHeight,
      window.innerHeight || 0
    );
    this.viewportWidth = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );
    this.presetImages = presetImages;

    this.options = options;
    this.defaults = {
      offsetRotation: 0,
      offsetScale: .6,
      offsetX: 0.0,
      offsetY: 0.0,
      radius: this.viewportWidth > this.viewportHeight ? this.viewportWidth : this.viewportHeight,
      slices: 28,
      zoom: 1,
      currentIndex: 0,
      rotation: 0,
    };

    this.options = Object.assign(this, this.defaults, this.options);

    if (this.domElement == null) {
      this.domElement = document.createElement("canvas");
      this.domElement.width = window.innerWidth;
      this.domElement.height = window.innerHeight;
    }
    if (this.context == null) {
      this.context = this.domElement.getContext("2d");
    }
    if (this.image == null) {
      this.image = new Image();
      this.image.onload = () => this.draw;
      this.image.src = this.presetImages[0];
    }
  }

  draw() {
    this.context.fillStyle = this.context.createPattern(this.options.image, "repeat");
    const step = this.TWO_PI / this.options.slices;
    const cx = this.options.image.width / 2;
    const results = [];
    for (
      let index = 0, i = 0, ref = this.options.slices;
      0 <= ref ? i <= ref : i >= ref;
      index = 0 <= ref ? ++i : --i
    ) {
      this.context.save();
      this.context.translate(window.innerWidth / 2, window.innerHeight / 2);
      this.context.rotate(index * step);
      this.context.beginPath();
      this.context.moveTo(-0.5, -0.5);
      this.context.arc(0, 0, this.options.radius, step * -0.5000001, step * 0.5000001);
      this.context.lineTo(0.5, 0.5);
      this.context.closePath();
      this.context.rotate(this.HALF_PI);
      this.context.scale([-1, 1][index % 2], 1);
      this.context.rotate(this.options.offsetRotation);
      this.context.translate(this.options.offsetX - cx, this.options.offsetY);
      this.context.scale(this.options.offsetScale, this.options.offsetScale);
      this.context.fill();
      results.push(this.context.restore());
    }
    return results;
  };

  nextImage() {
    let nextImagePath;
    if (this.options.currentIndex === this.presetImages.length - 1) {
      nextImagePath = this.presetImages[0];
      this.options.currentIndex = 0;
    } else {
      nextImagePath = this.presetImages[++this.options.currentIndex];
    }
    this.image.src = nextImagePath;
    return nextImagePath;
  }

  saveToPNG() {
    var link = document.createElement('a');
    link.setAttribute('download', `kaleidoscope-${Date.now()}.png`);
    link.setAttribute('href', this.domElement.toDataURL('image/png').replace("image/png", "image/octet-stream"));
    link.click();
  }
}