import dat from 'dat.gui';
import {interval} from "rxjs";
import {tap, throttleTime} from "rxjs/operators";
import Hammer from 'hammerjs';

import config from '../config.dev';
import HashNavigation from './hash-navigation';
import Kaleidoscope from './kaleidoscope';
import DragDrop from './drag-drop';

import '../css/reset.css';
import '../css/main.css';

import image1 from '../images/pic0.jpg';
import image2 from '../images/pic1.png';
import image3 from '../images/sploosh0045.png';

const hash = new HashNavigation();

const presetImages = [image1, image2, image3];

const options = {
  slices: 28,
  animate: true,
  cycleImages: false,
  cycleOffset: true,
  ease: 0.1,
  animationSpeedY: .2,
  animationSpeedX: .1,
  rotation: 0,
};

const kaleidoscope = new Kaleidoscope(options, presetImages);

document.body.appendChild(kaleidoscope.domElement);

window.addEventListener('resize', () => {
  kaleidoscope.domElement.width = window.innerWidth;
  kaleidoscope.domElement.height = window.innerHeight;
});

new DragDrop(function(data) {
  return (kaleidoscope.image.src = data);
});

let tx = kaleidoscope.offsetX;
let ty = kaleidoscope.offsetY;
let tr = kaleidoscope.offsetRotation;

const hammertime = new Hammer(document.querySelector('body'));
hammertime.get('pan').set({ direction: Hammer.DIRECTION_ALL });
hammertime.on('pan', function(ev) {
  tx += ev.velocityX * 10;
  ty += ev.velocityY * 10;
});

interval(1000 / 60).pipe(
  tap(i => {
    const delta = tr - kaleidoscope.rotation;
    const theta = Math.atan2(Math.sin(delta), Math.cos(delta));
    kaleidoscope.offsetX += (tx - kaleidoscope.offsetX) * options.ease;
    kaleidoscope.offsetY += (ty - kaleidoscope.offsetY) * options.ease;
    kaleidoscope.offsetRotation +=
    (theta - kaleidoscope.offsetRotation) * options.ease;
    kaleidoscope.draw();
  }),
  tap(i => {
    if (options.animate) {
      ty -= options.animationSpeedY * Math.abs(Math.sin(i/500));
      tx -= options.animationSpeedX * Math.cos(i/500);
    }
  }),
  throttleTime(1000),
  tap(() => {
    if (config.env === 'dev') {
      hash.setVal(options);
    } else {
      hash.setVal({});
    }
  })
).subscribe();

const gui = new dat.GUI();

gui
  .add(kaleidoscope.options, "slices").name('Slices')
  .min(2)
  .max(50)
  .step(2);

gui
  .add(kaleidoscope.options, "rotation").name('Image rotation')
  .min(-2)
  .max(2);

gui
  .add(kaleidoscope.options, "offsetScale").name('Image zoom')
  .min(0.5)
  .max(1.5);

gui
  .add(options, "animationSpeedY").name('Y axis speed')
  .min(-1.)
  .max(1);

gui
  .add(options, "animationSpeedX").name('X axis speed')
  .min(-1.)
  .max(1);

gui.add(options, "animate").name('Play/Pause');
gui.add(kaleidoscope, "saveToPNG").name('Save as PNG');
gui.add(kaleidoscope, "nextImage").name('Next image');

gui.close();
