export default class HashNavigation {
  constructor() {
    this.vals = document.location.hash
    .replace("#/", "")
    .replace("#", "")
    .split("/");
  }

  getVal(index) {
    return this.vals[this.vals.indexOf(index) + 1];
  };

  setVal(obj) {
    document.location.hash = Object.values(obj).join('/');
  };
};