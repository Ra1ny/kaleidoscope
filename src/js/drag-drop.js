export default class DragDrop {
  constructor(callback, context, filter) {
    this.bind = function(fn, me) {
      return function() {
        return fn.apply(me, arguments);
      };
    };
    let disable;
    this.callback = callback;
    this.context = context != null ? context : document;
    this.filter = filter != null ? filter : /^image/i;
    this.onDrop = this.bind(this.onDrop, this);
    disable = function(event) {
      event.stopPropagation();
      return event.preventDefault();
    };
    this.context.addEventListener("dragleave", disable);
    this.context.addEventListener("dragenter", disable);
    this.context.addEventListener("dragover", disable);
    this.context.addEventListener("drop", this.onDrop, false);
  }

  onDrop(event) {
    let file, reader;
    event.stopPropagation();
    event.preventDefault();
    file = event.dataTransfer.files[0];
    if (this.filter.test(file.type)) {
      reader = new FileReader();
      reader.onload = (function(_this) {
        return function(event) {
          return typeof _this.callback === "function"
          ? _this.callback(event.target.result)
          : void 0;
        };
      })(this);
      return reader.readAsDataURL(file);
    }
  };
}